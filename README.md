API Block
api/register - create new user, returns token for app 
(json: {
"name": "Serg",
"email": "sergvod@gmail.com",
"password": "123456789",
"password_confirmation": "123456789"
})
api/login - login user, returns token for app
(json: {
"email": "sergvod@gmail.com",
"password": "123456789"
})

api/createUserNote - create note for user, need token
(json: {
"user_id": 14,
"note_text": "test test"
})

Web block
http://localhost/login - login user, redirect to user list
http://localhost/register - create new user and login, redirect to user list

