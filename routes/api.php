<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register',         [ApiController::class, 'registerUser']);
Route::post('/login',            [ApiController::class, 'loginUser']);
Route::post('/logout',           [ApiController::class, 'logOut']);
Route::post('/createUserNote',   [ApiController::class, 'createUserNote'])
    ->middleware('api');

Route::post('/getUsersList', [UserController::class, 'getUsersList']);
Route::post('/getUser/{id}', [UserController::class, 'getUser']);
