<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if($user)
                        <form action="{{ url('storeUser') }}" method="post">
                            <input type="hidden" name="id" value="{{ $user->id }}">

                            <div class="row">
                                <label for="name">User Name</label>
                                <input name="name" id="name" class="form-group"
                                       value="{{ $user->name }}">
                            </div>

                            <div class="row">
                                <label for="email">User Email</label>
                                <input name="email" id="email" class="form-group"
                                       value="{{ $user->email }}">
                            </div>

                            {{ csrf_field() }}

                            <input type="submit" class="btn btn-success">
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
