<?php

namespace App\Http\Controllers;

use App\Models\Note;
use App\Models\User;
use App\Repository\BaseRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{
    private $noteRepo;

    public function __construct()
    {
        $this->noteRepo = new BaseRepository(new Note());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function registerUser(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        Auth::login($user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]));

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;

        return response()->json(['token' => $token]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function loginUser(Request $request): JsonResponse
    {
        $request->validate([
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8',
        ]);

        if (Auth::attempt($request->only('email', 'password'))) {
            $token = Auth::user()->createToken('Laravel Password Grant Client')
                ->accessToken;

            return response()->json(['success' => true, 'token' => $token]);
        } else {
            return response()->json(['success' => false, 'token' => 0]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function logOut(): JsonResponse
    {
        Auth::logout();

        return response()->json(['success' => true, 'token' => 0]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createUserNote(Request $request): JsonResponse
    {
        $model = $this->noteRepo->create($request->only('user_id', 'note_text'));

        if ($model) {
            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false]);
        }
    }
}
