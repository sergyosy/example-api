<?php
namespace App\Http\Controllers;

use App\Models\User;
use App\Repository\BaseRepository;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController
{
    /**
     * @var BaseRepository
     */
    private $userRepo;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->userRepo = new BaseRepository(new User());
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getUsersList()
    {
        return view('users')->with(['users' => $this->userRepo->getAll()]);
    }

    public function getUser(int $id)
    {
        return view('singleUser')
            ->with(['user' => $this->userRepo->getOneById($id)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeUser(Request $request)
    {
        $this->userRepo->update($request->only('id', 'name', 'email'));

        return redirect('getUsersList');
    }
}
