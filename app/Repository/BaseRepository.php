<?php
namespace App\Repository;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param Model $inputModel
     */
    public function __construct(Model $inputModel)
    {
        $this->model = $inputModel;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * @param int $id
     * @return User
     */
    public function getOneById(int $id): User
    {
        return $this->model->where('id', $id)->first();
    }

    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        $this->model->fill($data)->save();

        return $this->model;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function update(array $data): bool
    {
        return $this->getOneById($data['id'])->update($data);
    }
}
