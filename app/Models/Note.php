<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = 'user_note';

    protected $fillable = [
        'user_id',
        'note_text'
    ];

    public $timestamps = false;
}
